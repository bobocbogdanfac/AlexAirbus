﻿// Copyrigth (c) S.C.SoftLab S.R.L.
// All Rigths reserved.

namespace Jet3UpHelpers
{
    /// <summary>
    /// Possible fontSizes available in the machine.
    /// </summary>
    public enum FontSizeEnum
    {
        ISO1_5x3,
        ISO1_7x5,
        ISO1_9x7,
    }
}
