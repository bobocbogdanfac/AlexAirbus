﻿// Copyrigth (c) S.C.SoftLab S.R.L.
// All Rigths reserved.

namespace Jet3UpHelpers.Resources
{
    public enum Jet3UpStatusMessageType
    {
        Marked,
        Error,
        Done
    }
}
