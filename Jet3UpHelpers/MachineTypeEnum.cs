﻿// Copyrigth (c) S.C.SoftLab S.R.L.
// All Rigths reserved.

namespace Jet3UpHelpers
{
    /// <summary>
    /// Type of paint used by the machine.
    /// White paint needs bigger writing because it will be used on bigger pieces.
    /// ALba = White
    /// Neagra = Black.
    /// </summary>
    public enum MachineTypeEnum
    {
        Alba,
        Neagra
    }
}
