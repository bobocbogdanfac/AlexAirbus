﻿// Copyrigth (c) S.C.SoftLab S.R.L.
// All Rigths reserved.

namespace Aerotec.Resources.Helper
{
    /// <summary>
    /// Possible writing sizes required by the client.
    /// </summary>
    internal enum SizeEnum
    {
        Mic,
        Standard,
        Mare
    }
}
